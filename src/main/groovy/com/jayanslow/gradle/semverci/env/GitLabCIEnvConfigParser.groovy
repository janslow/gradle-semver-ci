package com.jayanslow.gradle.semverci.env

import java.util.function.Function

/**
 * Produces a CIEnvConfig iff running on GitLab CI, otherwise null.
 */
class GitLabCIEnvConfigParser implements Function<Map<String, String>, CIEnvConfig> {
  @Override
  CIEnvConfig apply(Map<String, String> env) {
    if (!env['GITLAB_CI']) {
      return null
    }
    def tag = env['CI_COMMIT_TAG'] ?: null
    return [
            id    : env['CI_PIPELINE_ID'] as int,
            tag   : tag,
            branch: tag ? null : env['CI_COMMIT_REF_NAME'],
    ] as CIEnvConfig
  }
}
