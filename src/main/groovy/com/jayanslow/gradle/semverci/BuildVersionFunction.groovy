package com.jayanslow.gradle.semverci

import com.jayanslow.gradle.semverci.env.CIEnvConfig
import com.jayanslow.gradle.semverci.env.GitLabCIEnvConfigParser
import de.skuzzle.semantic.Version
import org.gradle.api.InvalidUserDataException
import org.slf4j.LoggerFactory

import java.util.function.Function

/**
 * Function which accepts a base version and produces a build specific version.
 */
class BuildVersionFunction implements Function<Version, Version> {

  private static CIEnvConfig getEnv() {
    def env = System.getenv()
    def parsers = [new GitLabCIEnvConfigParser()]
    return parsers.findResult {it.apply(env)}
  }

  private CIEnvConfig env

  private VersionConfig config

  private logger = LoggerFactory.getLogger('semver-ci')

  BuildVersionFunction(final VersionConfig config) {
    this(config, getEnv())
  }

  BuildVersionFunction(final VersionConfig config, final CIEnvConfig env) {
    this.config = config
    this.env = env
  }

  Version apply(Version base) {
    if (base.preRelease != 'local') {
      throw new InvalidUserDataException("Base version must have the prerelease identifier 'local'; was '${base.preRelease}'")
    }
    def version = base.withBuildMetaData('')

    if (!env) {
      logger.info "CI not detected; using prerelease identifier 'local'"
      version = version.withPreRelease('local')
    }
    else if (env.tag && config.release.test(env.tag)) {
      logger.info "CI tag detected (${env.tag}); no prerelease identifiers"
      version = version.withPreRelease('')
    }
    else if (env.branch && config.releaseCandidate.test(env.branch)) {
      logger.info "CI RC branch detected (${env.branch}); 'rc' prerelease identifiers"
      version = version.withPreRelease("rc.${env.id}")
    }
    else if (env.branch && config.development.test(env.branch)) {
      logger.info "CI development branch detected (${env.branch}); 'dev' prerelease identifiers"
      version = version.withPreRelease("dev.${env.id}")
    }
    else {
      logger.info "Other CI branch detected (${env.branch}); 'ci' prerelease identifiers"
      def ref = env.tag ?: env.branch
      def cleanRef = ref.toLowerCase().replaceAll(~/[^a-z0-9.]+/, '.')
      version = version.withPreRelease("ci.${cleanRef}.${env.id}")
    }
    return version
  }
}
