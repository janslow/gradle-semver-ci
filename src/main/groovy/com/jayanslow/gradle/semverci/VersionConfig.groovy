package com.jayanslow.gradle.semverci

import java.util.function.Predicate

/**
 * Configuration to test type of ref.
 */
class VersionConfig {
  Predicate<String> development
  Predicate<String> releaseCandidate
  Predicate<String> release
}
