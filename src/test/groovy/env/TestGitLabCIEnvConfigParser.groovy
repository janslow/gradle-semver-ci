package env

import com.jayanslow.gradle.semverci.env.CIEnvConfig
import com.jayanslow.gradle.semverci.env.GitLabCIEnvConfigParser
import org.junit.Before
import org.junit.Test

/**
 *
 */
class TestGitLabCIEnvConfigParser {
  GitLabCIEnvConfigParser sut

  @Before
  void setUp() throws Exception {
    sut = new GitLabCIEnvConfigParser()
  }

  @Test
  void testNotGitLab() throws Exception {
    assert sut.apply([:]) == null
    assert sut.apply(['foo': 'bar']) == null
    assert sut.apply(['CI': 'true']) == null
    assert sut.apply(['GITLAB_CI': '']) == null
    assert sut.apply(['CI_PIPELINE_ID': '123', 'CI_BUILD_REF_NAME': 'foo']) == null
  }

  @Test
  void testBranch() throws Exception {
    def actual = sut.apply(['GITLAB_CI': 'true', 'CI_PIPELINE_ID': '123', 'CI_COMMIT_REF_NAME': 'a random branch'])
    assert actual instanceof CIEnvConfig
    assert actual.id == 123
    assert actual.branch == 'a random branch'
    assert actual.tag == null
  }

  @Test
  void testTag() throws Exception {
    def actual = sut.apply(['GITLAB_CI': 'true', 'CI_PIPELINE_ID': '321', 'CI_BUILD_REF_NAME': 'my tag', 'CI_COMMIT_TAG': 'my tag'])
    assert actual instanceof CIEnvConfig
    assert actual.id == 321
    assert actual.branch == null
    assert actual.tag == 'my tag'
  }
}
