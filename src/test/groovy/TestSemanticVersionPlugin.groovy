import com.jayanslow.gradle.semverci.SemanticVersionPlugin
import com.jayanslow.gradle.semverci.SemanticVersionPluginExtension
import com.jayanslow.gradle.semverci.VersionConfig
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Before
import org.junit.Test

/**
 *
 */
class TestSemanticVersionPlugin {
  SemanticVersionPlugin sut

  @Before
  void setUp() throws Exception {
    sut = new SemanticVersionPlugin()
  }

  @Test
  void testNormalize() throws Exception {
    def PATTERN_FOO = ~/^foo/
    def PATTERN_BAR = ~/bar/

    assert sut.normalize(null) == null
    assert sut.normalize([]) == []
    assert sut.normalize(PATTERN_FOO) == [PATTERN_FOO]
    assert sut.normalize([PATTERN_FOO]) == [PATTERN_FOO]
    assert sut.normalize([PATTERN_FOO, PATTERN_BAR]) == [PATTERN_FOO, PATTERN_BAR]

    def actual1 = sut.normalize('foo')
    assert actual1.size() == 1
    assert actual1[0].pattern() == '^\\Qfoo\\E$'

    def actual2 = sut.normalize(['foo', PATTERN_BAR])
    assert actual2.size() == 2
    assert actual2[0].pattern() == '^\\Qfoo\\E$'
    assert actual2[1] == PATTERN_BAR
  }

  @Test
  void testParse() throws Exception {
    VersionConfig actual = sut.parse([
            development     : null,
            releaseCandidate: 'foo',
            release         : [~/bar/, 'foo']
    ] as SemanticVersionPluginExtension)
    assert actual.development == null
    assert actual.releaseCandidate.size() == 1
    assert actual.releaseCandidate[0].pattern() == '^\\Qfoo\\E$'
    assert actual.release.size() == 2
    assert actual.release[0].pattern() == 'bar'
    assert actual.release[1].pattern() == '^\\Qfoo\\E$'
  }

  @Test
  void testApply() throws Exception {
    def project = ProjectBuilder.builder().build()
    project.pluginManager.apply 'com.jayanslow.gradle.semver-ci'
  }
}
