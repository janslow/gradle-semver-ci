# Changelog

## 0.3.2
* Update plugin urls.

## 0.3.1
* Improve user error handling

## 0.3.0
* ~breaking Use `version` property as base version, instead of current value.

## 0.2.0
* ~breaking Update GitLab environment variables (requires GitLab 9.0 or later)

## 0.1.2
* Initial public version