# CI Build Version

Updates the current build's [semantic version](http://semver.org/) based on the current CI context. Currently supports GitLab CI. 

## Version Logic
The generated versions have the following [precendence](http://semver.org/#spec-item-11) (from highest to lowest) based on git ref:
1. Tag which is a semantic version  (release version).
  * It must match the base version (with no prerelease identifiers).
2. `release`/`hotfix` branches (prerelease starts with `rc`).
3. `master`/`develop` branches (prerelease starts with `dev`).
4. Other branches and tags (prerelease starts with `ci`).

If used outside a CI environment, the prerelease is always `local`.

## Usage
```properties
# gradle.properties
version = 0.1.1-local # Prerelease identifiers must be exactly `local`
```

```groovy
// build.gradle
plugins {
  id 'com.jayanslow.gradle.semver-ci' version ''
}
```

It will then sets `Project#version` of the current project (and all it's subprojects) to the new version on plugin evaluation.

## Branch/tag names
It is possible to override the regexes used to match branch/tag names using the following plugin extension

```groovy
// build.gradle
semver {

}
```